terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "Knowledge21"

    workspaces {
      name = "terraform"
    }
  }
}

module "k8s-csd" {
  source = "./k8s-csd"
  token  = var.digitalocean_token
}

